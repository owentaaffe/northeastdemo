﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(NortheastDemo.Startup))]
namespace NortheastDemo
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
